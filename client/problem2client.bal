import ballerina/http;
import ballerina/io;

    // Creates a new client with the backend URL.
    final http:Client clientEndpoint = check new ("http://localhost:4000");
    json result = [];
public function main() returns error? {


    check LoginUI(clientEndpoint);
}

function LoginUI(http:Client clientEndpoint) returns error? {
    io:println("****************Welcome to Problem2 *****************");
    io:println("1. Get stats");
    io:println("2. Get by Region ");
    io:println("3. Update stats");
    io:println("****************************************");
    string choice = io:readln("Enter choice 1-3: ");

    match choice {
        "1" => {
            getAllRecords(); 
        }

        "2" => {
            filterRecords();

        }

        "3" => {
            updateRecords();

        }

        _ => {
           
        }

    }


    check LoginUI(clientEndpoint);

}

function getAllRecords() {
     
        result = checkpanic clientEndpoint->post("/graphql", {query: "{ all { region confirmed_cases  tested date } }"});
        io:println(result.toJsonString());
}

function filterRecords() {
        string region_code = io:readln("Enter Region : ");
        result = checkpanic clientEndpoint->post("/graphql", {query: "{ filter(region: \"" + region_code + "\" ) { region confirmed_cases recovered deaths } }"});
        io:println(result.toJsonString());
}

function updateRecords() {
           string region = io:readln("Enter Region : ");
        string cases = io:readln("Enter confirmed cases: ");
        string deaths = io:readln("Enter deaths: ");
        string recovery = io:readln("Enter recoveries: ");
        string tested = io:readln("Enter tested: ");

        result = checkpanic clientEndpoint->post("/graphql", {query: "mutation{ update(region: \""+region+"\", entry: {deaths: "+deaths+", recovered: "+recovery+", confirmed_case: "+cases+" , tested: "+tested+"}) { region confirmed_cases recovered deaths  tested } }"});
        io:println(result.toJsonString());
}