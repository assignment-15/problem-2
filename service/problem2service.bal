import ballerina/time;
import ballerina/graphql;



public distinct service class CovidData {
    private final readonly & CovidDetails entryRecord;

    function init(CovidDetails entryRecord) {
        self.entryRecord = entryRecord.cloneReadOnly();
    }
    resource function get region() returns string {
        return self.entryRecord.region;
    }

    resource function get confirmed_cases() returns int? {
        return self.entryRecord.confirmed_cases;
    }

    resource function get deaths() returns int? {
        return self.entryRecord.deaths;
    }

    resource function get recovered() returns int? {
        return self.entryRecord.recovered;
    }

    resource function get tested() returns int? {
        return self.entryRecord.tested;
    }

    resource function get date() returns string {
        return self.entryRecord.date;
    }
}

public type Entry record {|
    int deaths = 0;
    int recovered = 0;
    int confirmed_case = 0;
    int tested = 0;
|};

service /graphql on new graphql:Listener(4000) {
    resource function get all() returns CovidData[] {
        CovidDetails[] covidEntries = covidEntriesTable.toArray().cloneReadOnly();
        return covidEntries.map(entry => new CovidData(entry));
    }

    resource function get filter(string region) returns CovidData? {
        CovidDetails? covidEntry = covidEntriesTable[region];
        if (covidEntry is CovidDetails) {
            return new (covidEntry);
        }
        return;
    }

    remote function update(string region, Entry entry) returns CovidData {
        CovidDetails update_details = covidEntriesTable.get(region);
        time:Utc utc1 = time:utcNow();
        time:Utc utc2 = checkpanic time:utcFromString( utc1.toString() );
 
         update_details.date = utc2.toString();

        if (entry.confirmed_case > 0) {
            update_details.confirmed_cases += entry.confirmed_case;
        }

        if (entry.deaths > 0) {
            update_details.deaths += entry.deaths;
        }

        if (entry.recovered > 0) {
            update_details.recovered += entry.recovered;
        }

        if (entry.tested > 0) {
            update_details.tested += entry.tested;
        }

        return new CovidData(update_details);
    }
}
public type CovidDetails record {|
    readonly string region;
    int confirmed_cases = 0;
    int deaths = 0;
    int recovered = 0;
    int tested = 0;
    string date = "";
|};
//adding covid information to a table 
table<CovidDetails> key(region) covidEntriesTable = table [
        { date : "2022-09-28",region: "Khomas", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0 },
        { date : "2022-09-28", region: "Oshana", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        { date : "2022-09-28",region: "Omusati", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        { date : "2022-09-28", region: "Hardap", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        {  date : "2022-09-28",region: "Okavango East", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        {  date : "2022-09-28",region: "Okavango West", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        {  date : "2022-09-28",region: "Erongo", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        { date : "2022-09-28",region: "Otjozondjupa", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0},
        {  date : "2022-09-28",region: "Zambezi", confirmed_cases: 0, deaths: 0, recovered: 0, tested: 0}
    ];